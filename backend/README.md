# Ecommerce

Ecommerce is a RESTful Application of implementation of Joyjet backend test.

### Features Used

- Java 8
- Apache Maven
- Wildfly 10 (Application Server)
- Jackson 2.6
- JUnit 4

### Deploying Application

1 Clone project;
```sh
$ git clone https://bitbucket.org/msantosfilho/tech-interview
```

2 Open project folder;
```sh
$ cd ecommerce
```

3 With maven installed put in your terminal:
```sh
$ mvn eclipse:eclipse
$ mvn package
```

4 Get ecommerce.war in target folder and deploy in Wildfly;

5 Start Application Server;

6 Verify the deployment by navigating to your server address in your preferred browser;
```sh
http://127.0.0.1:8080/ecommerce/v1.0/purchases
```

### Issues Found

In Level 3 the output.json file has wrong results. Cart 1 the total sum is wrong, the correct result is 2375.

### Final Considerations

I did prefer keep the sums without truncate. This solution has intent to keep data integrity without data loss.
