package com.joyjet.ecommerce.factory;

import com.joyjet.ecommerce.interfaces.ICalculateDiscount;
import com.joyjet.ecommerce.strategy.CalculateDirectDiscount;
import com.joyjet.ecommerce.strategy.CalculatePercentDiscount;
import com.joyjet.ecommerce.strategy.CalculateWithoutDiscount;
import com.joyjet.ecommerce.to.request.DiscountRequestTO;

public abstract class CalculateDiscountFactory {

	public static ICalculateDiscount createCalculator(DiscountRequestTO discountRequestTO) {
		
		switch (discountRequestTO.getType()) {
		case "amount":
			return new CalculateDirectDiscount(discountRequestTO.getValue());
		case "percentage":
			return new CalculatePercentDiscount(discountRequestTO.getValue());
		default:
			return new CalculateWithoutDiscount();
		}
		
	}

}
