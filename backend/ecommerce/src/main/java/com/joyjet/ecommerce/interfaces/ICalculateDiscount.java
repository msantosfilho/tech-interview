package com.joyjet.ecommerce.interfaces;

import java.math.BigDecimal;

public interface ICalculateDiscount {

	BigDecimal calculate(BigDecimal totalItem);
	
}
