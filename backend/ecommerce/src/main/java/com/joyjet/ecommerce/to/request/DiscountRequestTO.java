package com.joyjet.ecommerce.to.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DiscountRequestTO {

	@JsonProperty("article_id")
	private Integer articleId;

	private String type;

	private Double value;

	public DiscountRequestTO() {
		super();
	}

	public DiscountRequestTO(Integer articleId, String type, Double value) {
		super();
		this.articleId = articleId;
		this.type = type;
		this.value = value;
	}

	public Integer getArticleId() {
		return articleId;
	}

	public void setArticleId(Integer articleId) {
		this.articleId = articleId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

}
