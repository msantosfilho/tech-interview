package com.joyjet.ecommerce.strategy;

import java.math.BigDecimal;

import com.joyjet.ecommerce.interfaces.ICalculateDiscount;

public class CalculateDirectDiscount implements ICalculateDiscount {

	private Double discountValue;

	public CalculateDirectDiscount(Double discountValue) {
		super();
		this.discountValue = discountValue;
	}

	@Override
	public BigDecimal calculate(BigDecimal totalItem) {
		return totalItem.subtract(new BigDecimal(discountValue));
	}

}
