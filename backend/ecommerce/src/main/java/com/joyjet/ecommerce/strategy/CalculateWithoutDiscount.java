package com.joyjet.ecommerce.strategy;

import java.math.BigDecimal;

import com.joyjet.ecommerce.interfaces.ICalculateDiscount;

public class CalculateWithoutDiscount implements ICalculateDiscount{

	@Override
	public BigDecimal calculate(BigDecimal totalItem) {
		return totalItem;
	}

}
