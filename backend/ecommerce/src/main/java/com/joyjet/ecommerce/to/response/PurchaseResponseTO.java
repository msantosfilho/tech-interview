package com.joyjet.ecommerce.to.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PurchaseResponseTO {

	private List<CartResponseTO> carts;
	
	public PurchaseResponseTO() {
		super();
	}

	public PurchaseResponseTO(List<CartResponseTO> cartsResponseTO) {
		this.carts = cartsResponseTO;
	}

	@JsonProperty("carts")
	public List<CartResponseTO> getCartsResponseTO() {
		return carts;
	}

	public void setCartsResponseTO(List<CartResponseTO> cartsResponseTO) {
		this.carts = cartsResponseTO;
	}
	
}
