package com.joyjet.ecommerce.to.request;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PurchaseRequestTO {

	private List<ArticleRequestTO> articles;
	
	private List<CartRequestTO> carts;
	
	@JsonProperty("delivery_fees")
	private List<DeliveryFeeRequestTO> deliveryFees;
	
	private List<DiscountRequestTO> discounts;

	public List<ArticleRequestTO> getArticles() {
		return articles;
	}

	public void setArticles(List<ArticleRequestTO> articles) {
		this.articles = articles;
	}

	public List<CartRequestTO> getCarts() {
		return carts;
	}

	public void setCarts(List<CartRequestTO> carts) {
		this.carts = carts;
	}

	public List<DeliveryFeeRequestTO> getDeliveryFees() {
		return deliveryFees;
	}

	public void setDeliveryFees(List<DeliveryFeeRequestTO> deliveryFees) {
		this.deliveryFees = deliveryFees;
	}

	public List<DiscountRequestTO> getDiscounts() {
		return discounts;
	}

	public void setDiscounts(List<DiscountRequestTO> discounts) {
		this.discounts = discounts;
	}
	
}
