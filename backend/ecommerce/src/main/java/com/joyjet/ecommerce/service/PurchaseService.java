package com.joyjet.ecommerce.service;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.joyjet.ecommerce.business.PurchaseBusiness;
import com.joyjet.ecommerce.to.request.PurchaseRequestTO;
import com.joyjet.ecommerce.to.response.PurchaseResponseTO;

@Path("/purchases")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class PurchaseService {

	@Inject
	private PurchaseBusiness purchaseBusiness;
	
	@POST
	@Path("/")
	public Response closeShopping(PurchaseRequestTO purchaseRequestTO){
		PurchaseResponseTO purchaseResponseTO = purchaseBusiness.processPurchase(purchaseRequestTO);
		return Response.ok(purchaseResponseTO).build();
	}
	
	public Response closePurchase(PurchaseRequestTO purchaseRequestTO){
		PurchaseResponseTO purchaseResponseTO = purchaseBusiness.processPurchase(purchaseRequestTO);
		return Response.ok(purchaseResponseTO).build();
	}
	
}
