package com.joyjet.ecommerce.factory;

import java.math.BigDecimal;

import com.joyjet.ecommerce.to.request.DeliveryFeeRequestTO;
import com.joyjet.ecommerce.to.request.EligibleTransactionVolumeRequestTO;

public abstract class PurchaseUtil {

	public static Boolean verifyIntervalPrice(DeliveryFeeRequestTO deliveryFeeRequestTO, BigDecimal totalItems) {
		EligibleTransactionVolumeRequestTO eligibleTransactionVolumeRequestTO = deliveryFeeRequestTO
				.getEligibleTransactionVolume();
		return eligibleTransactionVolumeRequestTO.getMinPrice() < totalItems.doubleValue()
				&& eligibleTransactionVolumeRequestTO.getMaxPrice() > totalItems.doubleValue();
	}

	public static Boolean compareArticleId(Integer articleId, Integer compareArticleId) {
		return articleId.equals(compareArticleId);
	}

}
