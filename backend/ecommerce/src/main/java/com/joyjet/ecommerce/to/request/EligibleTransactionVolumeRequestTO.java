package com.joyjet.ecommerce.to.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EligibleTransactionVolumeRequestTO {

	@JsonProperty("min_price")
	private Double minPrice;
	
	@JsonProperty("max_price")
	private Double maxPrice;

	public EligibleTransactionVolumeRequestTO() {
		super();
	}

	public EligibleTransactionVolumeRequestTO(Double minPrice, Double maxPrice) {
		super();
		this.minPrice = minPrice;
		this.maxPrice = maxPrice;
	}

	public Double getMinPrice() {
		return minPrice;
	}

	public void setMinPrice(Double minPrice) {
		this.minPrice = minPrice;
	}

	public Double getMaxPrice() {
		return maxPrice;
	}

	public void setMaxPrice(Double maxPrice) {
		this.maxPrice = maxPrice;
	}
	
}
