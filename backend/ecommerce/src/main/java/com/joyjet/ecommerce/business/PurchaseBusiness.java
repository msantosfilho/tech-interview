package com.joyjet.ecommerce.business;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.enterprise.context.Dependent;

import com.joyjet.ecommerce.factory.CalculateDiscountFactory;
import com.joyjet.ecommerce.factory.PurchaseUtil;
import com.joyjet.ecommerce.to.request.ArticleRequestTO;
import com.joyjet.ecommerce.to.request.CartItemRequestTO;
import com.joyjet.ecommerce.to.request.CartRequestTO;
import com.joyjet.ecommerce.to.request.DeliveryFeeRequestTO;
import com.joyjet.ecommerce.to.request.DiscountRequestTO;
import com.joyjet.ecommerce.to.request.PurchaseRequestTO;
import com.joyjet.ecommerce.to.response.CartResponseTO;
import com.joyjet.ecommerce.to.response.PurchaseResponseTO;

@Dependent
public class PurchaseBusiness {

	private PurchaseRequestTO purchaseRequestTO;

	public PurchaseBusiness() {
		super();
	}

	public PurchaseBusiness(List<ArticleRequestTO> articlesRequestTO, List<DeliveryFeeRequestTO> deliveryFeesRequestTO,
			List<DiscountRequestTO> discountsRequestTO) {
		super();
		this.purchaseRequestTO = new PurchaseRequestTO();
		this.purchaseRequestTO.setArticles(articlesRequestTO);
		this.purchaseRequestTO.setDeliveryFees(deliveryFeesRequestTO);
		this.purchaseRequestTO.setDiscounts(discountsRequestTO);
	}

	public PurchaseResponseTO processPurchase(PurchaseRequestTO purchaseRequestTO) {
		this.purchaseRequestTO = purchaseRequestTO;
		List<CartResponseTO> cartsResponseTO = processCart(purchaseRequestTO.getCarts());
		return new PurchaseResponseTO(cartsResponseTO);
	}

	public List<CartResponseTO> processCart(List<CartRequestTO> cartsRequestTO) {
		List<CartResponseTO> cartsResponse = cartsRequestTO.stream().map(cart -> buildCartResponse(cart))
				.collect(Collectors.toList());
		return cartsResponse;
	}

	private CartResponseTO buildCartResponse(CartRequestTO cart) {
		CartResponseTO cartResponseTO = new CartResponseTO();
		cartResponseTO.setId(cart.getId());
		BigDecimal calculateTotalItems = calculateTotalItems(cart.getItems());
		cartResponseTO.setTotal(calculateTotalItemsWithDelivery(calculateTotalItems).doubleValue());
		return cartResponseTO;
	}

	public BigDecimal calculateTotalItems(List<CartItemRequestTO> cartItemsRequestTO) {
		BigDecimal totalItems = cartItemsRequestTO.stream().map(cartItem -> calculateTotalItem(cartItem))
				.reduce(BigDecimal.ZERO, BigDecimal::add);
		return totalItems;
	}

	public BigDecimal calculateTotalItem(CartItemRequestTO cartItem) {
		Optional<ArticleRequestTO> optional = purchaseRequestTO.getArticles().stream()
				.filter(article -> PurchaseUtil.compareArticleId(article.getId(), cartItem.getArticleId()))
				.findFirst();
		if (optional.isPresent()) {
			BigDecimal totalItem = new BigDecimal(cartItem.getQuantity() * optional.get().getPrice());
			return calculateDiscount(totalItem, cartItem);
		}
		return BigDecimal.ZERO;
	}

	public BigDecimal calculateTotalItemsWithDelivery(BigDecimal totalItems) {
		List<DeliveryFeeRequestTO> deliveryFeesResquestTO = purchaseRequestTO.getDeliveryFees();
		Optional<DeliveryFeeRequestTO> optional = deliveryFeesResquestTO.stream()
				.filter(deliveryFee -> PurchaseUtil.verifyIntervalPrice(deliveryFee, totalItems))
				.findFirst();
		if (optional.isPresent())
			return new BigDecimal(totalItems.doubleValue() + optional.get().getPrice());
		else
			return totalItems;
	}

	public BigDecimal calculateDiscount(BigDecimal totalItem, CartItemRequestTO cartItem) {
		List<DiscountRequestTO> discountsRequestTO = purchaseRequestTO.getDiscounts();
		Optional<DiscountRequestTO> optional = discountsRequestTO.stream()
				.filter(discount -> PurchaseUtil.compareArticleId(discount.getArticleId(), cartItem.getArticleId()))
				.findFirst();
		if (optional.isPresent()) {
			DiscountRequestTO discountRequestTO = optional.get();
			cartItem.setCalculateDiscount(CalculateDiscountFactory.createCalculator(discountRequestTO));
			return cartItem.getCalculateDiscount().calculate(totalItem);
		}
		return totalItem;
	}

}