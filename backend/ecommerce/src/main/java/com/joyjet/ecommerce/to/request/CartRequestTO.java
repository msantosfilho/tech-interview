package com.joyjet.ecommerce.to.request;

import java.util.List;

public class CartRequestTO {

	private Integer id;

	private List<CartItemRequestTO> items;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<CartItemRequestTO> getItems() {
		return items;
	}

	public void setItems(List<CartItemRequestTO> items) {
		this.items = items;
	}

}
