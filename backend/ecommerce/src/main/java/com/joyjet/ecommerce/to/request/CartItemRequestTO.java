package com.joyjet.ecommerce.to.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.joyjet.ecommerce.interfaces.ICalculateDiscount;

public class CartItemRequestTO {

	@JsonProperty("article_id")
	private Integer articleId;

	private Integer quantity;
	
	@JsonIgnore
	private ICalculateDiscount calculateDiscount;
	
	public CartItemRequestTO() {
		super();
	}

	public CartItemRequestTO(Integer articleId, Integer quantity) {
		super();
		this.articleId = articleId;
		this.quantity = quantity;
	}

	public Integer getArticleId() {
		return articleId;
	}

	public void setArticleId(Integer articleId) {
		this.articleId = articleId;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public ICalculateDiscount getCalculateDiscount() {
		return calculateDiscount;
	}

	public void setCalculateDiscount(ICalculateDiscount calculateDiscount) {
		this.calculateDiscount = calculateDiscount;
	}

}
