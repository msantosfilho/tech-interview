package com.joyjet.ecommerce.to.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DeliveryFeeRequestTO {

	@JsonProperty("eligible_transaction_volume")
	private EligibleTransactionVolumeRequestTO eligibleTransactionVolume;

	private Double price;

	public DeliveryFeeRequestTO() {
		super();
	}

	public DeliveryFeeRequestTO(EligibleTransactionVolumeRequestTO eligibleTransactionVolume, Double price) {
		super();
		this.eligibleTransactionVolume = eligibleTransactionVolume;
		this.price = price;
	}

	public EligibleTransactionVolumeRequestTO getEligibleTransactionVolume() {
		return eligibleTransactionVolume;
	}

	public void setEligibleTransactionVolume(EligibleTransactionVolumeRequestTO eligibleTransactionVolume) {
		this.eligibleTransactionVolume = eligibleTransactionVolume;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

}
