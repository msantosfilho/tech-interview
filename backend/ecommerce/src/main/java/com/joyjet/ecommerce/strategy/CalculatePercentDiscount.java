package com.joyjet.ecommerce.strategy;

import java.math.BigDecimal;

import com.joyjet.ecommerce.interfaces.ICalculateDiscount;

public class CalculatePercentDiscount implements ICalculateDiscount {

	private static final BigDecimal ONE_HUNDRED = new BigDecimal(100);
	
	private Double discountValue;
	
	public CalculatePercentDiscount(Double discountValue) {
		super();
		this.discountValue = discountValue;
	}

	@Override
	public BigDecimal calculate(BigDecimal totalItem) {
		BigDecimal valuePercent = totalItem.multiply(new BigDecimal(discountValue)).divide(ONE_HUNDRED) ;
		return totalItem.subtract(valuePercent);
	}

}
