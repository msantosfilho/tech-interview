package com.joyjet.ecommerce.test;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.joyjet.ecommerce.business.PurchaseBusiness;
import com.joyjet.ecommerce.to.request.ArticleRequestTO;
import com.joyjet.ecommerce.to.request.CartItemRequestTO;
import com.joyjet.ecommerce.to.request.DeliveryFeeRequestTO;
import com.joyjet.ecommerce.to.request.DiscountRequestTO;
import com.joyjet.ecommerce.to.request.EligibleTransactionVolumeRequestTO;

public class PurchaseBusinessTest {
	
	private List<ArticleRequestTO> articlesRequestTO;
	private List<CartItemRequestTO> cartItemsRequestTO;
	private List<DeliveryFeeRequestTO> deliveryFeesRequestTO;
	private List<DiscountRequestTO> discountsRequestTO;
	private PurchaseBusiness business;
	
	@Before
	public void initilize(){
		ArticleRequestTO article1 = new ArticleRequestTO(1, "water", 100.0);
		ArticleRequestTO article2 = new ArticleRequestTO(2, "honey", 200.0);
		ArticleRequestTO article3 = new ArticleRequestTO(3, "mango", 400.0);
		ArticleRequestTO article4 = new ArticleRequestTO(4, "tea", 1000.0);
		articlesRequestTO = Arrays.asList(article1, article2, article3, article4);
		
		CartItemRequestTO cartItem1 = new CartItemRequestTO(1, 6);
		CartItemRequestTO cartItem2 = new CartItemRequestTO(2, 2);
		CartItemRequestTO cartItem3 = new CartItemRequestTO(4, 1);
		cartItemsRequestTO = Arrays.asList(cartItem1, cartItem2, cartItem3);
		
		EligibleTransactionVolumeRequestTO eligibleTransactionVolume1 = new EligibleTransactionVolumeRequestTO(0.0, 1000.0);
		EligibleTransactionVolumeRequestTO eligibleTransactionVolume2 = new EligibleTransactionVolumeRequestTO(1000.0, 2000.0);
		EligibleTransactionVolumeRequestTO eligibleTransactionVolume3 = new EligibleTransactionVolumeRequestTO(2000.0, null);
		
		DeliveryFeeRequestTO deliveryFee1 = new DeliveryFeeRequestTO(eligibleTransactionVolume1, 800.0);
		DeliveryFeeRequestTO deliveryFee2 = new DeliveryFeeRequestTO(eligibleTransactionVolume2, 400.0);
		DeliveryFeeRequestTO deliveryFee3 = new DeliveryFeeRequestTO(eligibleTransactionVolume3, 0.0);
		deliveryFeesRequestTO = Arrays.asList(deliveryFee1, deliveryFee2, deliveryFee3);
		
		DiscountRequestTO discount1 = new DiscountRequestTO(2, "amount", 25.0);
		DiscountRequestTO discount2 = new DiscountRequestTO(5, "percentage", 30.0);
		discountsRequestTO = Arrays.asList(discount1, discount2);
		
		business = new PurchaseBusiness(this.articlesRequestTO, this.deliveryFeesRequestTO, this.discountsRequestTO);
	}

	@Test
	public void calculateTotalItem(){
		assertEquals(new BigDecimal(600.0), business.calculateTotalItem(cartItemsRequestTO.get(0)));
	}
	
	@Test
	public void calculateTotalItems(){
		assertEquals(new BigDecimal(1975.0), business.calculateTotalItems(cartItemsRequestTO));
	}
	
	@Test
	public void calculateItemWithArticleNotInList(){
		CartItemRequestTO cartItem = new CartItemRequestTO(99, 3);
		cartItemsRequestTO = Arrays.asList(cartItem);
		assertEquals(BigDecimal.ZERO, business.calculateTotalItem(cartItemsRequestTO.get(0)));
	}
	
	@Test
	public void calculateTotalItemsWithDelivery(){
		BigDecimal totalItems = new BigDecimal(800.0);
		assertEquals(new BigDecimal(1600.0), business.calculateTotalItemsWithDelivery(totalItems));
	}
	
}
